#+TITLE: Sensor simulator ray-tracer

A spectral path tracer (Monte Carlo integration, physically based,
multiple importance sampling) that can simulate both vision and LIDAR,
implemented in the GPGPU purely functional array programming language
[[https://futhark-lang.org/][Futhark]].

See the [[file:Ray_Tracing_for_Sensor_Simulation_using_Parallel_Functional_Programming.pdf][thesis]] for background and a high-level overview of how the
raytracer works.

[[file:prism-dispersion.png]]

* Dependencies
  Regardless of platform, you need to install both [[https://futhark.readthedocs.io/en/latest/installation.html][Futhark]] and [[https://rustup.rs/][Rust]]
  (and a C compiler). Futhatk is used for the main implementation, and
  all code that runs on the GPU, while Rust and C are used for some
  host-side code, like loading object files and showing an SDL2
  window.

** SDL2
   On Linux, statically compiled SDL2 is included in [[./deps/SDL2/]].

   On Windows you'll have to install SDL2 yourself. Using MSYS2
   (mingw) is recommended (yay pacman!). You may have to update your
   environment variables so that the linker can find the installed
   libraries.

** OpenCL
   You'll need both the libraries and headers for compilation, and
   drivers with support at runtime.

   On Linux, simply install the appropriate package (something like
   "opencl-dev") to get the development library. Depending on your
   distribution and hardware, your drivers may be OpenCL ready out of
   the box. Otherwise you must install packages that provide an OpenCL
   runtime (and an ICD loader?).

   On Windows, get an OpenCL SDK (preferably a "light" version) from
   your GPU vendor -- for AMD, [[https://github.com/GPUOpen-LibrariesAndSDKs/OCL-SDK/releases][you can find it here]]. With regards to
   runtime, you should be good to go as long as you've installed the
   normal GPU drivers for your system.

* Build
  Simply run ~make~. If it doesn't work, try poking around in the
  Makefile. You may need to change ~CC=clang~ to ~CC=gcc~ for example.

* Run
  The compilation produces two binaries: ~main-interactive~ and
  ~main-save~. One is an interactive demo, the other is a standalone
  run-once program that renders the scene once in either camera or
  lidar mode. Actually, the ~save~ demo hasn't been updated since
  adding support for config files, so it doesn't work right now, but
  it did around commit ~26d2a6da~, ca. June 9, 2020.

  On Linux, run with ~./main-interactive~. On Windows, use just
  ~main-interactive.exe~ instead.

  #+BEGIN_EXAMPLE
  $ ./main-interactive -c camera.toml
  Using OpenCL device: GeForce GTX 1060 6GB
  no of triangles: 2188
  #+END_EXAMPLE

** Controls
   For the interactive demo:

   - w, a, s, d, x, z :: Translation.
   - Arrow keys :: Rotation.
   - 1, 2 :: Increase / decrease resolution.
   - q, e :: Decrease / increase samples per (virtual) pixel.
   - SPACE :: Toggle accumulation.
   - n, m :: Disable / enable accumulation.
   - i, k :: Increase / decrease aperture size.
   - o, l :: Increase / decrease focal distance.
   - t :: Toggle LIDAR sensor.
   - 8, 9, 0 :: Self-transmitter: flash / scanning / none.
   - p :: Toggle sky.
* Project structure
  The raytracer itself is written in Futhark, but there's not a little
  C and Rust code necessary as well for interfacing with
  Futhark. Futhark by itself can only produce a pure program that
  takes an input and returns an output -- it can't read files, open
  windows, draw graphics on the screen, etc. That's what the host-code
  written in C and Rust handles.

  - ~assets/~ :: Testing scenes. ~.obj~ object geometry files and
                 associated ~.mtl~ material files. We use nonstandard
                 extensions to ~.mtl~, so don't expect these objects
                 to look good in other engines or even open at all.
  - ~build/~ :: Build artifacts. Don't mind these.
  - ~deps/~ :: Vendored dependencies. Only includes a statically
               compiled SDL2 for Linux at the moment. If your
               platform has an SDL2 package, using that would
               probably be preferred instead.
  - ~lib/~ :: The automatically downloaded Futhark depencies
              specified in ~futhark.pkg~
  - ~ljus/~ :: Rust code for loading scenes (~*.obj~ and ~*.mtl~) and
               reading the config file (~*.toml~).
  - ~src/~ :: The Futhark source of the raytracer itself. This code
              is what will run on the GPU.
  - ~{camera,lidar}.toml~ :: Example config files for the raytracer in
       the TOML format.

** Futhark source
   Many parts of the raytracer are heavily based on the [[www.pbr-book.org/3ed-2018/contents.html][PBR Book]]

   In ~src/~, we have these files:

   - ~bvh.fut~ :: The Bounding Volume Hierarchy. Specifically, a
                  Linear BVH, based on Karras's 2012
                  paper. ~radix_tree.fut~ implements the main
                  algorithm necessary, and ~bvh.fut~ specifically
                  converts the general radix tree into a BVH.
   - ~camera.fut~ :: The ~camera~ type, and related functions. A
                     ~camera~ has a position and orientation, and some
                     more fields. Includes functions for manipulatin
                     the camera geometry, and for generating rays and
                     transmitters given a ~camera~.
   - ~common.fut~ :: Miscellaneous helper functions and such, like the
                     ~maybe~ type for representing optional values.
   - ~direct.fut~ :: Functions for computing the direct
                     lighting. Occlusion testing, Multiple Importance
                     Sampling (MIS), etc. Basically implements chapter [[http://www.pbr-book.org/3ed-2018/Light_Transport_I_Surface_Reflection/Direct_Lighting.html][14.3
                     of PBR book]].
   - ~integrator.fut~ :: The "outer" loops of the pathtracing
        integration. ~sample_pixels~ runs the pathtracing function for
        each pixel in the window, and ~path_trace~ samples one whole
        path of radiance, iterating over the sampled points along the
        path and appending the direct radiance for each. Basically
        implements [[http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration.html][chapter 13]] and [[http://www.pbr-book.org/3ed-2018/Light_Transport_I_Surface_Reflection/Direct_Lighting.html][chapter 14.3]] of PBR book.
   - ~lib.fut~ :: The entrypoint functions to the raytracer. The
                  functions marked ~entry~ in here are the ones you
                  can call from C/Rust. ~init~ initializes the
                  raytracer, ~resize~ handles a window resize event,
                  ~step~ runs one step for the interactive mode, ~key~
                  handles an SDL2 key event, ~render~ converts the
                  frame of path samples in the state to a frame of
                  color values, ready to be loaded and blitted to the
                  screen on the host side.
   - ~light.fut~ :: A few functions related to sampling a point on a
                    light source, for use in the direct lighting
                    integration. Implements some of the stuff in [[http://www.pbr-book.org/3ed-2018/Light_Sources.html][PBR
                    book chapter 12]].
   - ~linalg.fut~ :: Some linear algebra types and functions. Vectors
                     and such.
   - ~material.fut~ :: Functions related to the Bidirectional
                       Scattering Distribution Function (BSDF). In
                       short, the BSDF is a distribution function that
                       takes an incident vector, an exitant vector,
                       and the surface properties, and returns how
                       much light is reflected/refracted along that
                       junction. The ~*_f~ and ~*_bsdf~ functions
                       compute exactly this, while the ~*_sample_dir~
                       functions *sample* an incident direction given
                       a surface and exitant direction. So where the
                       distribun of the BSDF is big, it's higher to
                       sample an incident vector in those
                       directions. The ~*_bsdf~ functions are used
                       when doing Multiple Importance Sampling, while
                       the ~*_sample_dir~ functions are used for
                       sampling scattering directions to compute the
                       indirect lighting in ~path_trace~. Basically an
                       implementation of PBR book [[http://www.pbr-book.org/3ed-2018/Reflection_Models.html][chapter 8]], [[http://www.pbr-book.org/3ed-2018/Materials.html][chapter
                       9]], [[http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration.html][chapter 13,]] and [[http://www.pbr-book.org/3ed-2018/Light_Transport_I_Surface_Reflection/Sampling_Reflection_Functions.html][chapter 14.1]].
   - ~radix_tree.fut~ :: The backing algorithms for constructing a
        BVH. Implementation of the algorithm in Karras's 2012 paper
        "Maximizing parallelism in the construction of BVHs, octrees,
        and k-d trees".
   - ~rand.fut~ :: Some convenience functions for random sampling for
                   example a coordinate in a $[(0, 0), (1, 1))$
                   square.
   - ~scene.fut~ :: Contains the ~scene~ and ~accel_scene~ types
                    (~accel_scene~ is like scene, but the geometry has
                    been put in a BVH and been
                    "accelerated"). Includes some functions for
                    parsing raw data to the Futhark ~scene~ type, and
                    a couple of convenience function for interacting
                    with a ~{accel_}scene~.
   - ~sdl.fut~ :: SDL keycode constants.
   - ~shapes.fut~ :: Geometry/shape stuff. The geometric ~ray~ type,
                     the ~triangle~ type, the raycasting ~hit~ type,
                     and raycasting functions for computing
                     ray/geometry intersections. Basically implements
                     PBR book [[http://www.pbr-book.org/3ed-2018/Shapes.html][chapter 3]].
   - ~spectrum.fut~ :: Functions and types related to light
                       spectrums. We mostly arrived at these
                       ourselves, but they also partly implement
                       what's described in PBR book [[http://www.pbr-book.org/3ed-2018/Color_and_Radiometry/The_SampledSpectrum_Class.html][chapter 5.2]].
   - ~state.fut~ :: Just contains the ~state~ type, which is the type
                    that contains all program state between frames,
                    returned by the ~init~ function and necessary
                    particularly for remembering state between
                    ~step~:s in the interactive demo.

* Attributions
  Much of the windowing code in the interactive demo is based on [[https://github.com/diku-dk/lys][lys]],
  owned by DIKU and licensed under the ISC license (see
  [[./demo-interactive/LYS-LICENSE]]).

  Our LBVH implementation takes a lot of inspiration from [[https://github.com/athas/raytracingthenextweekinfuthark][the one in
  here by Athas]].

* License
  This project is released under the AGPL, version 3 or later. See
  [[./LICENSE]]. Volvo gets an exception!

** Notice
   Copyright (C) 2020  Ari von Nordenskjöld & Johan Johansson

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU Affero General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public
   License along with this program.  If not, see
   <https://www.gnu.org/licenses/>.
