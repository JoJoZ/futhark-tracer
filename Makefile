BACKEND=opencl
CC=clang

PROG_FUT_DEPS:=$(shell find lib -name \*.fut) $(wildcard src/*.fut)

NOWARN_CFLAGS=-std=c11 -O2
CFLAGS=$(NOWARN_CFLAGS)  -Wall -Wextra -Wconversion -pedantic -DLYS_BACKEND_$(BACKEND)
LDFLAGS=-lm -lfreetype -lpthread
INCLUDE=-I./build

ifeq ($(OS),Windows_NT)
OPENCL_LDFLAGS = -L"${OCL_ROOT}\lib\x86_64" -L"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\lib\x64" -lOpenCL
OPENCL_INCLUDE = -I"${OCL_ROOT}\include" -I"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\include"
else
OPENCL_LDFLAGS = -lOpenCL
OPENCL_INCLUDE =
endif

ifeq ($(BACKEND),opencl)
	LDFLAGS += $(OPENCL_LDFLAGS)
	INCLUDE += $(OPENCL_INCLUDE)
else ifeq ($(BACKEND),cuda)
	LDFLAGS += -lcuda -lnvrtc -L/chalmers/sw/sup64/cuda_toolkit-10.1.243/cuda-toolkit/lib64 -L"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\lib\x64"
	INCLUDE += -I/chalmers/sw/sup64/cuda_toolkit-10.1.243/cuda-toolkit/include -I"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\include"
else ifeq ($(BACKEND),c)
# nothing
else
	$(error Unknown BACKEND: $(BACKEND).  Must be 'opencl', 'cuda', or 'c')
endif

ifeq ($(OS),Windows_NT)
	LDFLAGS += -lmingw32 -lSDL2main -lSDL2 -lSDL2_ttf -lWs2_32 -lUserenv
	EXE=.exe
else
	NOWARN_CFLAGS += -fPIC
	LDFLAGS += -L./deps/SDL2/lib -ldl -lSDL2 -lSDL2_ttf
	INCLUDE += -I./deps/SDL2/include
	EXE=
endif

.PHONY: all
all: main-interactive$(EXE) main-save$(EXE) libtracer.a

.PHONY: lib
lib: libtracer.a

main-interactive$(EXE): demo-interactive/liblys.c demo-interactive/liblys.h demo-interactive/file-watcher.h libtracer.a build/libljus.a
	$(CC) demo-interactive/liblys.c build/libljus.a libtracer.a -o $@ $(CFLAGS) -I./demo-interactive $(INCLUDE) $(LDFLAGS)

main-save$(EXE): $(shell find demo-save/src -name \*.rs) demo-save/Cargo.toml demo-save/.cargo/config libtracer.a
	cd demo-save && cargo build --release && cp target/release/demo-save$(EXE) ../main-save$(EXE)

libtracer.a: build/tracer.o
	ar rcs libtracer.a build/tracer.o
	touch demo-save/src/ffi.rs

# We do not want warnings and such for the generated code.
build/tracer.o: build/tracer.c
	$(CC) -o $@ -c $< $(NOWARN_CFLAGS) $(INCLUDE)

build/tracer.c: src/lib.fut futhark.pkg $(PROG_FUT_DEPS)
	test futhark.pkg -nt lib && futhark pkg sync; \
	mkdir -p build && futhark $(BACKEND) -o build/tracer --library src/lib.fut

build/tracer.h: build/tracer.c

build/libljus.a: $(shell find ljus/src -name \*.rs) ljus/Cargo.toml
	cd ljus && cargo build --lib --release && cp target/release/libljus.a ../build/libljus.a

run: main-interactive$(EXE)
	./main-interactive$(EXE) -o assets/SpectrumSphere.obj

clean:
	rm -rf libtracer.a main-interactive$(EXE) main-save$(EXE) build

clean-full: clean
	cd ljus && cargo clean
	cd demo-save && cargo clean
