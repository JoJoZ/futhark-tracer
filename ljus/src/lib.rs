#![feature(vec_into_raw_parts)]

use std::convert::TryInto;
use std::ffi::{CStr,CString};
use std::io::Read;
use std::iter::{once, repeat};
use std::path::Path;
use toml::Value;

const RED_WAVELEN: f32 = 610.0;
const GREEN_WAVELEN: f32 = 550.0;
const BLUE_WAVELEN: f32 = 460.0;



#[no_mangle]
pub extern "C" fn load_obj_data(
    obj_path: *const i8,
    num_tris: *mut usize,
    num_mat_components: *mut usize,
    tri_data: *mut *mut f32,
    tri_mats: *mut *mut u32,
    mat_data: *mut *mut f32,
) {
    unsafe {
        let obj_path = Path::new(CStr::from_ptr(obj_path).to_str().unwrap());
        let (ts, tms, ms) = load(obj_path);
        let (ts_ptr, _, _) = ts.into_raw_parts();
        let (tm_ptr, n_tris, _) = tms.into_raw_parts();
        let (ms_ptr, n_mat_components, _) = ms.into_raw_parts();
        *num_tris = n_tris;
        *num_mat_components = n_mat_components;
        *tri_data = ts_ptr;
        *tri_mats = tm_ptr;
        *mat_data = ms_ptr;
    }
}
#[no_mangle] pub unsafe extern "C" fn free_obj_data(tri_data: *mut f32, tri_mats: *mut u32, mat_data: *mut f32) {
    Box::from_raw(tri_data);
    Box::from_raw(tri_mats);
    Box::from_raw(mat_data);
}

pub fn load(obj_path: &Path) -> (Vec<f32>, Vec<u32>, Vec<f32>) {
    let (models, materials) = tobj::load_obj(obj_path).expect("Load obj file");
    let (mut tris, mut tri_mats) = (Vec::new(), Vec::new());
    for mesh in models.into_iter().map(|m| m.mesh) {
        let mat_ix = mesh.material_id.expect("Mesh doesn't have material");
        let vertices = mesh.positions.chunks(3).collect::<Vec<_>>();
        for tri_is in mesh.indices.chunks(3) {
            tri_mats.push(mat_ix as u32);
            for &v_i in tri_is {
                tris.extend(vertices[v_i as usize].iter().cloned());
            }
        }
    }
    let mut mats = Vec::with_capacity(materials.len());
    for m in materials {
        let color_old = m.diffuse;
        let color = get_spectrum(&m, "Sp").unwrap_or([
            RED_WAVELEN,
            color_old[0],
            GREEN_WAVELEN,
            color_old[1],
            BLUE_WAVELEN,
            color_old[2],
            -1.0,
            0.0,
            -1.0,
            0.0,
            -1.0,
            0.0,
        ]);
        let roughness = get_scalar(&m, "Pr").unwrap_or(1.0);
        let metalness = get_scalar(&m, "Pm").unwrap_or(0.0);
        let ref_ix = m.optical_density;
        let opacity = get_scalar(&m, "Tf").unwrap_or(1.0);
        let emission_old = get_vec3(&m, "Ke").unwrap_or([0.0, 0.0, 0.0]);
        let emission = get_spectrum(&m, "Em").unwrap_or([
            RED_WAVELEN,
            emission_old[0],
            GREEN_WAVELEN,
            emission_old[1],
            BLUE_WAVELEN,
            emission_old[2],
            -1.0,
            0.0,
            -1.0,
            0.0,
            -1.0,
            0.0,
        ]);
        let mut mat = [0.0; 28];
        let mat_it = color
            .iter()
            .chain(once(&roughness))
            .chain(once(&metalness))
            .chain(once(&ref_ix))
            .chain(once(&opacity))
            .chain(emission.iter());
        for (i, &x) in mat_it.enumerate() {
            mat[i] = x
        }
        mats.push(mat);
    }
    println!("no of triangles: {:?}", tris.len() / 9);
    (tris, tri_mats, mats.concat())
}

fn parse_scalar(s: &str) -> f32 {
    s.parse::<f32>().expect("Scalar parameter")
}

fn parse_vec(s: &str) -> Vec<f32> {
    s.split_whitespace()
        .map(|t| t.parse::<f32>().unwrap())
        .collect::<Vec<_>>()
}

fn parse_vec3(s: &str) -> [f32; 3] {
    let v = parse_vec(s);
    if v.len() != 3 {
        panic!("Expected 3-vector parameter")
    } else {
        [v[0], v[1], v[2]]
    }
}

fn get_scalar(m: &tobj::Material, field: &str) -> Option<f32> {
    m.unknown_param.get(field).map(|s| parse_scalar(&s))
}

fn get_vec3(m: &tobj::Material, field: &str) -> Option<[f32; 3]> {
    m.unknown_param.get(field).map(|s| parse_vec3(&s))
}

fn get_spectrum(m: &tobj::Material, field: &str) -> Option<[f32; 12]> {
    m.unknown_param.get(field).map(|s| {
        let v = parse_vec(s);
        let filler = repeat(&[-1.0, 0.0]).flat_map(|x| x);
        let mut v12 = [0.0; 12];
        for (i, &x) in v.iter().chain(filler).take(12).enumerate() {
            v12[i] = x;
        }
        v12
    })
}

#[derive(Debug)]
#[repr(C)]
pub struct Config {
    obj_path: *mut i8,
    default_res_x: i16,
    default_res_y: i16,
    subsampling: i8,
    camera: Camera,
    transmitter: Transmitter,
    sensor: Sensor,
    rendermode: Rendermode
}

#[derive(Debug)]
#[repr(C)]
enum TransmitterType {
    None,
    Scanning{theta_degrees: f32},
    Flash{temperature: f32}
}

#[derive(Debug)]
#[repr(C)]
enum Rendermode {
    CAMERA,
    LIDAR
}


#[derive(Debug)]
#[repr(C)]
struct Camera {
    pitch: f32,
    yaw: f32,
    origin: [f32;3],
    field_of_view: f32,
    aperture: f32,
    focal_dist: f32
}

#[derive(Debug)]
#[repr(C)]
struct Transmitter {
    r#type: TransmitterType,
    radius: f32,
    intensity: f32,
}


#[derive(Debug)]
#[repr(C)]
struct Sensor {
    offset_radius: f32,
    channels: (*mut Channel, usize)
}

#[derive(Debug)]
#[repr(C)]
struct Channel {
    mu: f32,
    sigma: f32,
    display_as: [f32;3],
}

#[no_mangle]
pub extern "C" fn load_config(file_path: *const i8) -> Config {

    let file_path = unsafe {Path::new(CStr::from_ptr(file_path).to_str().unwrap())};
    let mut file = std::fs::File::open(file_path).unwrap();

    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    let parsed = contents.parse::<Value>().unwrap();

    Config{
        obj_path:
            CString::new(parsed["obj_path"].as_str().unwrap()).unwrap().into_raw(),
        default_res_x:
            parsed["default_res_x"].as_integer().unwrap() as i16,
        default_res_y:
            parsed["default_res_y"].as_integer().unwrap() as i16,
        subsampling:
            parsed["subsampling"].as_integer().unwrap() as i8,
        camera:
            Camera{
                pitch:
                    parsed["camera"]["pitch"].as_float().unwrap() as f32,
                yaw:
                    parsed["camera"]["yaw"].as_float().unwrap() as f32,
                origin:
                    parsed["camera"]["origin"].as_array().unwrap().iter()
                        .map(|v| v.as_float().unwrap() as f32)
                        .collect::<Vec<f32>>().as_slice().try_into().unwrap(),
                field_of_view:
                    parsed["camera"]["field_of_view"].as_float().unwrap() as f32,
                aperture:
                    parsed["camera"]["aperture"].as_float().unwrap() as f32,
                focal_dist:
                    parsed["camera"]["focal_dist"].as_float().unwrap() as f32,
            },
        transmitter:
            Transmitter{
                r#type:
                    match parsed["transmitter"]["type"].as_str().unwrap() {
                        "scanning" => 
                            TransmitterType::Scanning{
                                theta_degrees:
                                    parsed["transmitter"]["theta_degrees"].as_float().unwrap() as f32
                            },
                        "flash" => 
                            TransmitterType::Flash{
                                temperature:
                                    parsed["transmitter"]["temperature"].as_float().unwrap() as f32,
                            },
                         _ => TransmitterType::None
                    },
                radius: 
                    parsed["transmitter"]["radius"].as_float().unwrap() as f32,
                intensity: 
                    parsed["transmitter"]["intensity"].as_float().unwrap() as f32,
            },
        sensor:
            Sensor{
                offset_radius:
                    parsed["sensor"]["offset_radius"].as_float().unwrap() as f32,
                    channels: {
                        let (ptr, len, _) = parsed["sensor"]["channels"]
                            .as_array().unwrap()
                            .iter()
                            .map(|v| Channel { mu: v["mu"].as_float().unwrap() as f32,
                                               sigma: v["sigma"].as_float().unwrap() as f32,
                                               display_as: v["display_as"]
                                                   .as_array() .unwrap() .iter()
                                                   .map(|u| u.as_float().unwrap() as f32)
                                                   .collect::<Vec<f32>>().as_slice().try_into().unwrap() })
                            .collect::<Vec<_>>().into_raw_parts();
                        (ptr, len)
              }
            },
        rendermode: match parsed["rendermode"].as_str().unwrap() {
            "lidar"  => Rendermode::LIDAR,
            _        => Rendermode::CAMERA
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn free_config(config: Config) {
    Box::from_raw(config.sensor.channels.0);
}
