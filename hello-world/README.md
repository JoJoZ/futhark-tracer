# A basic example of calling Futhark from C

`lib.fut` contains the Futhark code. The function to be called from C
must be marked `entry` instead of `let`, and may only accept/return
simple types like the numeric types, and arrays/matrices of
these. Futhark structs can not be read from C, so those must be
destructured to arrays and tuples.

Compiling `lib.fut` with `futhark opencl --library lib.fut` creates a
`lib.c` file containing the generated OpenCL code and some additional
wrapping stuff. This file is not very introspectable. The `lib.h` file
contains the declarations of the functions we may call from
C. Particularly the `futhark_entry_*` are of interest, as those are
the functions we marked as `entry` in our `lib.fut`.

`main.c` contains a very simple program that initializes the Futhark
context, and executes our entrypoint function `hello_world` defined in
`lib.fut`.

Compile the executable with `gcc lib.c main.c -lOpenCL -lm`. The
OpenCL library may require you to pass the `-I` and `-L` flags with
the `include` and `lib` paths of where OpenCL is installed
respectively, depending on your platform and whether the environment
variables `INCLUDE` and `LIBRARY_PATH` are set. `-lm` tells gcc to
link with the math library, libm, which contains functions like `sin`
and `exp` etc.

Compile the whole shebang and run in a single line with

```
futhark opencl --library lib.fut && gcc lib.c main.c -lOpenCL -lm && ./a.out
```

Note: If your `INCLUDE` path is correct, but the compiler still
doesn't find the headers, for example if it says that "CL/cl.h" is
missing, try telling the compiler to search `INCLUDE` explicitly, like
`gcc foo.c -I"$INCLUDE"`.
