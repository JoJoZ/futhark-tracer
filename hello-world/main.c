#include "lib.h"

#include <stdint.h>
#include <stdio.h>

int main() {
    struct futhark_context_config* cfg = futhark_context_config_new();
    struct futhark_context* ctx = futhark_context_new(cfg);

    int32_t out;
    futhark_entry_hello_world(ctx, &out, 23);

    printf("The value is: %d\n", out);

    futhark_context_free(ctx);
    futhark_context_config_free(cfg);
    return 0;
}
