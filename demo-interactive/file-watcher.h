// ***************************************************************************
// Windows doesn't have inotify for file watching, so just disable live reload.
// ***************************************************************************
#ifdef _WIN32

#include <stdio.h>

typedef struct { int dummy; } Watcher;

Watcher watch(const char* dirpath, const char* filename) {
    (void)dirpath;
    (void)filename;
    printf("On windows. Using dummy file watcher. Live config reload not enabled.\n");
    Watcher w = { .dummy = 0 };
    return w;
}

void unwatch(Watcher w) {
    (void)w;
    return;
}

bool poll_changed(Watcher w) {
    (void)w;
    return false;
}

// ***************************************************************************
// On Linux, use inotify to support live reload of config
// ***************************************************************************
#else

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#define EVENT_BUF_N_EVENTS 128
#define NAME_MAX 1024
// Events are dynamically sized. The last field is an in-line array of
// length `.len`
#define EVENT_MAX_SIZE (sizeof(struct inotify_event) + NAME_MAX + 1)

typedef struct {
    int fd;
    int wd;
    const char* dirpath;
    const char* filename;
} Watcher;

Watcher watch(const char* dirpath, const char* filename) {
    int fd = inotify_init();
    if (fd < 0) {
        perror("inotify_init");
    }
    int wd = inotify_add_watch(fd, dirpath, IN_MODIFY | IN_CREATE);
    // Open in non-blocking mode. `read` will now immediately return
    // `EAGAIN` if there's no data available.
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) | O_NONBLOCK);
    Watcher w = { .fd = fd, .wd = wd, .dirpath = dirpath, .filename = filename };
    return w;
}

void unwatch(Watcher w) {
    (void) inotify_rm_watch(w.fd, w.wd);
    (void) close(w.fd);
}

bool poll_changed(Watcher w) {
    char buffer[EVENT_BUF_N_EVENTS * EVENT_MAX_SIZE];
    long length = read(w.fd, buffer, EVENT_BUF_N_EVENTS * EVENT_MAX_SIZE);
    if (length < 0) {
        if (errno == EAGAIN) { // No data available right now
            return false;
        } else {
            perror("read");
        }
    }
    long i = 0;
    while (i < length) {
        struct inotify_event* event = (struct inotify_event*) &buffer[i];
        i += sizeof(struct inotify_event) + event->len;
        if (event->len && (event->mask & (IN_CREATE | IN_MODIFY)) && (strcmp(event->name, w.filename) == 0)) {
            return true;
        }
    }
    return false;
}

#endif

// Example usage:
//
// int main(int argc, char **argv) {
//     Watcher w = watch(".", "test.c");
//     while (true) {
//         if (poll_changed(w)) {
//             printf("%s changed at unix time: %ld\n", w.filename, (long)time(NULL));
//         }
//         sleep(1);
//     }
//     unwatch(w);
//     return 0;
// }
