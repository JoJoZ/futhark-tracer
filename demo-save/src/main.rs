#![feature(extern_types)]
#![feature(vec_into_raw_parts)]
#![feature(clamp)]

mod ffi;
mod wrapper;

use pcd_rs::{DataKind, WriterBuilder};
use std::path::*;
use wrapper::*;

fn main() {
    let (lidar_mode, dims, n_samples, cam, scene) = get_args().unwrap_or_else(|| help_exit());
    if lidar_mode {
        let mut fut = Fut::init(2, dims, cam, scene);
        let points = fut.sample_points(n_samples);
        let mut writer =
            WriterBuilder::new(points.len() as u64, 1, Default::default(), DataKind::ASCII)
                .expect("new WriterBuilder")
                .create::<_, Vec3>("dump.pcd")
                .expect("created path");
        for point in points.iter() {
            writer.push(&point).unwrap();
        }
        writer.finish().unwrap();
    } else {
        let mut fut = Fut::init(0, dims, cam, scene);
        let data = fut.sample_image(n_samples);
        let bytes = data
            .into_iter()
            .map(|x| (x.clamp(0.0, 1.0) * 255.99) as u8)
            .collect::<Vec<u8>>();
        image::save_buffer("out.png", &bytes, dims.0, dims.1, image::ColorType::Rgb8).unwrap();
    }
}

fn get_args() -> Option<(bool, (u32, u32), u32, Cam, PathBuf)> {
    fn get_parse<F: std::str::FromStr>(args: &mut impl Iterator<Item = String>) -> Option<F> {
        args.next()?.parse().ok()
    }
    let mut args = std::env::args().skip(1);
    let lidar_mode = args.next()? == "lidar";
    let dims = (get_parse(&mut args)?, get_parse(&mut args)?);
    let n_samples = get_parse(&mut args)?;
    let cam = Cam {
        origin: Vec3 {
            x: get_parse(&mut args)?,
            y: get_parse(&mut args)?,
            z: get_parse(&mut args)?,
        },
        pitch: get_parse(&mut args)?,
        yaw: get_parse(&mut args)?,
        fov_deg: get_parse(&mut args)?,
    };
    let scene = PathBuf::from(args.next()?);
    Some((lidar_mode, dims, n_samples, cam, scene))
}

fn help_exit() -> ! {
    println!("Error parsing command line arguments.");
    println!("Expected: MODE WIDTH HEIGHT N_SAMPLES X Y Z PITCH YAW FOV SCENE");
    std::process::exit(1);
}
